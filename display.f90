module Display

  !!Contains the functions usefull for plotting datas
  implicit none

Contains

  subroutine WriteGnuPlot(x, y, nom_fichier)
    !Writes two vectors in a specified file. File will be owerwritten if
    !existing
    implicit none
    !Input :  + real vectors X,Y
    !         + filename nom_fichier
    real*8, dimension(:) :: x, y
    character(len=*) :: nom_fichier
    integer :: i
    open(unit = 24, file = nom_fichier)
    do i = 1, size(x)
       write(24, *) x(i), y(i)
    end do
    close(24)
  end subroutine

  function EntierToString(n0) result(chaine)
    !Convert an integer lower than 9999 into a string
    implicit none
    !Input :  + no : integer, must be < 10000
    !Outup :  + string
    integer, intent(in) :: n0
    character(len=4) :: chaine
    integer :: n

    n = n0
    chaine(4:4) = achar(mod(n,10)+48)
    n = n/10
    chaine(3:3) = achar(mod(n,10)+48)
    n = n/10
    chaine(2:2) = achar(mod(n,10)+48)
    n = n/10
    chaine(1:1) = achar(mod(n,10)+48)

  end function EntierToString

end module
