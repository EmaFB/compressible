module TransportSchemes


  implicit none

contains

  function GetInitialFunction(x0,L,type_init) result (U0)
    ! Compute the initial condition on a given point 
    !   respecting the periodics conditions
    implicit none
    !Inputs :   + real::L        = size of the interval  
    !           + real::x0       = given point
    !           + int::type_init = 1 for gaussian, 2 for crenel
    real*8,intent(in)::L,x0
    integer,intent(in)::type_init

    real*8::U0,x
    integer::i,nb_intervalle

    ! On commence par rammener x0 dans l'intervalle -L;L
    nb_intervalle=floor(abs(x0)/L)

    !Il faut ensuite soustraire à x : 
    ! L*nb  si nb intervalle pair et L*(nb+1) si intervalle impair
    ! De plus on utilise signe pour voir si on décalle a droite ou à gauche

    x=x0-sign(nb_intervalle+1*mod(nb_intervalle,2),floor(x0))*L 
    !Sign(a,b) renvoie a affecté du signe de b (il faut b entier car a entier donc floor)

    if(type_init==1) then
      U0=exp(-(x+5)**2)
    else if(type_init==2) then
      if (x>-7.5 .and. x<-2.5) then
        U0=1d0
      else
        U0=0d0
      end if
    else if (type_init == 3 ) then
      U0 = sin(x)
    end if

  end function GetInitialFunction

  subroutine periodic(U,n)
    implicit none
    !Apply peridic conditions : U(0) = U(N) and U(N+1) = U(1)
    ! for a given vector U of size N+2
    real*8,dimension(-1:n+2),intent(inout)::U
    integer,intent(in)::n

    U(-1) = U(n-1)
    U(0) = U(n)
    U(n+1) = U(1)
    U(n+2) = U(2)
  end subroutine

  function UpwindFlux(a,Ui,Ui_p)
    implicit none
    !Returns the flux calculated with Upwind scheme
    !Ui_p = U_i+1
    real*8,intent(in)::a,Ui,Ui_p
    real*8::UpwindFlux

    if (a > 0) then
      UpwindFlux = a*Ui
    else 
      UpwindFlux = a*Ui_p
    end if

  end function UpwindFlux

  function LWFlux(a,c,Ui,Ui_p)
    implicit none
    !Returns the flux calculated with Lax-Wendroff scheme
    !Ui_p = U_i+1
    real*8,intent(in)::a,c,Ui,Ui_p
    real*8::LWFlux

    LWFlux = 0.5 * a * (Ui + Ui_p - c * (Ui_p - Ui) )

  end function LWFlux

  function discret_max(Ui_next,Ui_m,Ui,Ui_p)
    implicit none
    real*8,intent(in)::Ui_next,Ui_m,Ui,Ui_p
    logical::discret_max

    real*8::Umax,Umin

    Umax = max (Ui_m,Ui,Ui_p)
    Umin = min (Ui_m,Ui,Ui_p)

    discret_max = (Ui_next >= Umin) .and. (Ui_next <= Umax)

  end function

end module TransportSchemes


