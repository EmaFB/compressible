module mathfunc

  implicit none

contains

  function lagrange(point,ordre,X,Fx)
  implicit none
  !Use the points X and the values at these points Fx to compute the 
  ! interpolation polynome. This polynom is evaluated at point x.
  !Caution: for a given order k, arrays X and Fx must be of size k+1

  !Inputs
  real*8,intent(in)::point
  integer,intent(in)::ordre
  real*8,dimension(ordre+1),intent(in)::X,Fx
  !Ouput
  real*8::lagrange

  real*8,dimension(ordre+1)::L
  integer::i,j

  L=1
  lagrange=0 
  !Lagrange basis
  do i=1, ordre+1
    do j=1,ordre+1
      if (j/=i) L(i) = L(i) * ( point-X(j) ) / ( X(i) - X(j) )
    end do
  end do

  !Calcul using langrage basis
  do i=1,ordre+1
    lagrange = lagrange + L(i)*FX(i)
  end do

  end function lagrange

  !!!!        Functions reconstrk       !!!!
  ! For a given cell Ki, the function reconstrk computes the reconstruction Wi at order
  ! k ( ie with a interpolation polynom of order k+1 )
  ! A vector of size 2 is returned. It stores, in this order: 
  !     Wi,-   --> Interpolation on cell Ki is used to compute this value on boundary xi-1/2
  !     Wi,+   --> Interpolation on cell Ki is used to compute this value on boundary xi+1/2

  function reconstr1(X,U,dx)
  ! The function needs the couple (Xi,Ui) at point i 
  !   and the spacing dx to calculate on points xi+1/2, xi-1/2
  implicit none
  !Inputs
  real*8::X,U    ! Only one value is needed
  real*8::dx
  !Output
  real*8,dimension(2)::reconstr1

  reconstr1(1) = lagrange (X-dx/2., 0, (/X/),(/U/) )
  reconstr1(2) = lagrange (X+dx/2., 0, (/X/),(/U/) )

  end function reconstr1

  function reconstr2(X,U,dx,method)
  ! This function, to compute inteerpolation on cells Ui needs values in
  ! cells Ki+1 and Ki-1. So 3 couples (Xi,Ui) are required
  ! Fist oder polynome in cell Ki can be computed with different methods:
  ! method = 'left'    --> uses points Xi-1 and Xi
  ! method = 'center'  --> uses points Xi-1 and Xi+1
  ! method = 'right'   --> uses points Xi and Xi+1
  implicit none
  !Inputs
  real*8,dimension(3)::X,U    ! Central value (i) is X(2),U(2)
  real*8::dx
  character(len=*)::method
  !Output
  real*8,dimension(2)::reconstr2

  select case(method)
  case('left','gauche','l','g')
    reconstr2(1) = lagrange ( X(2)-dx/2. ,1, (/X(1),X(2)/), (/U(1),U(2)/) )
    reconstr2(2) = lagrange ( X(2)+dx/2. ,1, (/X(1),X(2)/), (/U(1),U(2)/) )
  case('center','centre','c')
    reconstr2(1) = lagrange ( X(2)-dx/2. ,1, (/X(1),X(3)/), (/U(1),U(3)/) )
    reconstr2(2) = lagrange ( X(2)+dx/2. ,1, (/X(1),X(3)/), (/U(1),U(3)/) )
  case('right','droite','r','d')
    reconstr2(1) = lagrange ( X(2)-dx/2. ,1, (/X(2),X(3)/), (/U(2),U(3)/) )
    reconstr2(2) = lagrange ( X(2)+dx/2. ,1, (/X(2),X(3)/), (/U(2),U(3)/) )
  case default
    print*,'Fatal error : Not a valid method for 1st order interpolation'
    stop
  end select

  end function reconstr2

  function reconstr3(X,U,dx)
  ! This function, to compute interpolation on cells Ki needs values in
  ! cells Ki+1 and Ki-1. So 3 couple (Xi,Ui) are required
  implicit none
  !Inputs
  real*8,dimension(3)::X,U    ! Central value (i) is X(2),U(2)
  real*8::dx
  !Output
  real*8,dimension(2)::reconstr3

  reconstr3(1) = lagrange ( X(2)-dx/2. ,2, (/X(1),X(2),X(3)/), (/U(1),U(2),U(3)/) )
  reconstr3(2) = lagrange ( X(2)+dx/2. ,2, (/X(1),X(2),X(3)/), (/U(1),U(2),U(3)/) )
  
  end function reconstr3

  function reconstr4(X,U,dx,method)
  ! This function, to compute inteerpolation on cells Ui needs values in
  ! cells Ki+1 and Ki-1. So 4 couples (Xi,Ui) are required
  ! Third oder polynome in cell Ki can be computed with different methods:
  ! method = 'left'    --> uses points Xi,Xi-1,Xi+1 and Xi-2
  ! method = 'center'  --> uses points Xi-1,Xi+1,Xi-2 and Xi+2
  ! method = 'right'   --> uses points Xi,Xi-1,Xi+1 and Xi+2
  implicit none
  !Inputs
  real*8,dimension(5)::X,U    ! Central value (i) is X(3),U(3)
  real*8::dx
  character(len=*)::method
  !Output
  real*8,dimension(2)::reconstr4

  select case(method)
  case('left','gauche','l','g')
    reconstr4(1) = lagrange ( X(3)-dx/2. ,3, (/X(1),X(2),X(3),X(4)/), &
         (/U(1),U(2),U(3),U(4)/) )
    reconstr4(2) = lagrange ( X(3)+dx/2. ,3, (/X(1),X(2),X(3),X(4)/), &
         (/U(1),U(2),U(3),U(4)/) )
  case('center','centre','c')
    reconstr4(1) = lagrange ( X(3)-dx/2. ,3, (/X(1),X(2),X(4),X(5)/), &
         (/U(1),U(2),U(4),U(5)/) )
    reconstr4(2) = lagrange ( X(3)+dx/2. ,3, (/X(1),X(2),X(4),X(5)/), &
         (/U(1),U(2),U(4),U(5)/) )
  case('right','droite','r','d')
    reconstr4(1) = lagrange ( X(3)-dx/2. ,3, (/X(2),X(3),X(4),X(5)/), &
         (/U(2),U(3),U(4),U(5)/) )
    reconstr4(2) = lagrange ( X(3)+dx/2. ,3, (/X(2),X(3),X(4),X(5)/), &
         (/U(2),U(3),U(4),U(5)/) )
  case default
    print*,'Fatal error : Not a valid method for 1st order interpolation'
    stop
  end select
end function reconstr4

end module mathfunc
