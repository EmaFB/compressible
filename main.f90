program main

! use TransportSchemes
use Flux
use Display
use mathfunc

! Solve the general equation dt(u)+dx(f(u)) = 0
! on the domain [-L:L] with peridic conditions.
! Initial condition can be a gaussian function (type_init =1)
! or a crenel function (type_init = 2)
! Definition of the function f(u)  with type_function
! Type_function = 1 for the transport equation (f(u)=a*u)
! TYpe_function = 2 for Burger equation (f(u)=u*u/2)
implicit none
!!Parameters
real*8, parameter :: a_transp = 1.0d0
integer, parameter :: type_function = 1, type_init = 3
real*8, parameter :: tol_dm = 1e-14
!!  1. Spatial discretisation
integer, parameter :: N = 1000
real*8, parameter::L = 10.0d0 
!!  2. Temporal discretisation
real*8, parameter :: t_final = 10.0d0
real*8, parameter :: cfl = 0.7
!!  3. Display parameter
integer, parameter :: Ndisplay = 10

!!Variables
real*8 :: dx, dt, t 
real*8 :: c
integer :: nb_iter, nt
integer::i, j, k

real*8, dimension(-1:N + 2) :: X
real*8, dimension(N) :: U0, U_exacte
real*8, dimension(-1:N + 2) :: rec_order
real*8, dimension(-1:N + 2) :: U, U_next
real*8, dimension (-1:N + 2, 2) :: W1, W2, W3, W4


!Computes spacing, temporal step and number of iterations
dx = 2.0d0 * L / N
dt = cfl / abs(a_transp) * dx
nb_iter = ceiling(t_final / dt)
c = a_transp * dt / dx

! Spatial discretisation, initialisation and calcul of exact solution
do i = 1, N
   X(i) = -L + i * dx
   U0(i) = GetInitialFunction(X(i), L, type_init)
   U_exacte(i) = GetInitialFunction((X(i) - a_transp * nb_iter * dt), L, type_init)
end do
call WriteGnuPlot(X(1:N), U0, "Initial")
call WriteGnuPlot(X(1:N), U_exacte, "Exact")

! Computing solution using order 2 and 1 with Maximum discrete criterium
U(1:N) = U0
call periodic(U, N)
call periodic(X, N)
do nt = 1, nb_iter
   !--------------------------------------------TIME LOOP--------------------------------------------

   k = 0
   !Reconstructions
   do i = 1, N
      W1 (i, :) = reconstr1 ( X(i), U(i), dx)
      W2 (i, :) = reconstr2 ( X(i - 1:i + 1), U(i - 1:i + 1), dx, 'l')
      W3 (i, :) = reconstr3 ( X(i - 1:i + 1), U(i - 1:i + 1), dx)
      W4 (i, :) = reconstr4 ( X(i - 2:i + 2), U(i - 2:i + 2), dx, 'r')
   end do
   call periodic (W1(:, 1), N)
   call periodic (W1(:, 2), N)
   call periodic (W2(:, 1), N)
   call periodic (W2(:, 2), N)
   call periodic (W3(:, 1), N)
   call periodic (W3(:, 2), N)
   call periodic (W4(:, 1), N)
   call periodic (W4(:, 2), N)

   rec_order = 4
   do i = 1, N
      U_next(i) = U(i) - dt / dx * (GeneralFlux(W4(i, 2), W4(i + 1, 1), type_function, a_transp) &
      - GeneralFlux(W4(i - 1, 2), W4(i, 1), type_function, a_transp))
   end do
   call periodic(U_next, N)    !Periodic conditions
   call WriteGnuPlot(X(1:N), U(1:N), "Rec4")


   do i = 1, N
      ! if (.not. discret_max(U_next(i),U(i - 1), U(i),U(i + 1),tol_dm)) then
      ! if (.not. global_max(U_next(i), U)) then
      if (.not. u2(U_next(i), U(i), U(i - 2), U(i - 1), U(i + 1), U(i + 2), dx)) then
         do j = i - 1, i + 1
            !Third order calculation
            rec_order(j) = 3
            U_next(j) = U(j) - dt / dx * (GeneralFlux(W3(j, 2), W3(j + 1, 1), type_function, a_transp) &
            - GeneralFlux(W3(j - 1, 2), W3(j, 1), type_function, a_transp))
            !---------------Test Lineaire-------------
            !!$    		  U_next(j) = U(j) - dt/dx * ( UpwindFlux(a_transp,W2(j,2),W2(j+1,1)) &
            !!$                                  - UpwindFlux(a_transp,W2(j-1,2),W2(j,1)) )
            !-----------------------------------------
         end do
      end if
   end do
   call periodic(U_next, N)    !Periodic conditions

   do i = 1, N
      ! if (.not. discret_max(U_next(i), U(i - 1), U(i), U(i + 1),tol_dm)) then
      ! if (.not. global_max(U_next(i), U)) then
      if (.not. u2(U_next(i), U(i), U(i - 2), U(i - 1), U(i + 1), U(i + 2), dx)) then
         do j = i - 1, i + 1
            !Second order calculation
            rec_order(j) = 2
            U_next(j) = U(j) - dt / dx * (GeneralFlux(W2(j, 2), W2(j + 1, 1), type_function, a_transp) &
            - GeneralFlux(W2(j - 1, 2), W2(j, 1), type_function, a_transp))
         end do
      end if
   end do
   call periodic(U_next, N)    !Periodic conditions

   do i = 1, N
      ! if (.not. discret_max(U_next(i), U(i - 1), U(i), U(i + 1),tol_dm)) then
      ! if (.not. global_max(U_next(i), U)) then
      if (.not. u2(U_next(i), U(i), U(i - 2), U(i - 1), U(i + 1), U(i + 2), dx)) then
         do j = i - 1, i + 1
            !First order calculation
            rec_order(j) = 1
            U_next(j) = U(j) - dt / dx * (GeneralFlux(W1(j, 2), W1(j + 1, 1), type_function, a_transp) &
            - GeneralFlux(W1(j - 1, 2), W1(j, 1), type_function, a_transp))
            !---------------Test Lineaire-------------
            !!$    		  U_next(j) = U(j) - dt/dx * ( UpwindFlux(a_transp,W1(j,2),W1(j+1,1)) &
            !!$                                  - UpwindFlux(a_transp,W1(j-1,2),W1(j,1)) )
            !-----------------------------------------

         end do
      end if
   end do
   call periodic(U_next, N)    !Periodic conditions


   U = U_next
   if(mod(nt, Ndisplay) == 0) then
      print*, nt
      call WriteGnuPlot(X(1:N), U(1:N), "Animation/Mood3_2_1_" // trim(EntierToString(nt)) // ".dat")
      call WriteGnuPlot(X(1:N), Rec_order(1:N), "Animation_order/Mood3_2_1_" // trim(EntierToString(nt)) // ".dat")
   end if
   !--------------------------------------- END OF TIME LOOP------------------------------------------
end do
call WriteGnuPlot(X(1:N), U(1:N), "Final")
call WriteGnuPlot(X(1:N), Rec_order(1:N), "Rec_orders")
end program


