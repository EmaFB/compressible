module Flux
implicit none
contains

function GetSource(Ui,type_function,a)
implicit none
!Retourne la fonction f(U(t,x)) pour un shéma du type:
!dt(U) + dx(f(U))=0
real*8, intent(in) :: Ui
integer, intent(in) :: type_function !=1 pour le transport et =2 pour Burger
real*8, intent(in) :: a
real*8 :: GetSource
if (type_function==1) then
   GetSource=a*Ui
   elseif(type_function==2) then
   GetSource=Ui*Ui/2.
else
   print*,"Fatal Error : type de fonction indefini"
end if
end function GetSource

function GetDerivedSource(Ui,type_function,a)
implicit none
!Retourne la derivee de la fonction f(U(t,x)) pour un shéma du type:
!dt(U) + dx(f(U))=0
real*8, intent(in) :: Ui
integer, intent(in) :: type_function !=1 pour le transport et =2 pour Burger
real*8, intent(in) :: a
real*8 :: GetDerivedSource
if (type_function==1) then
   GetDerivedSource=a
   elseif(type_function==2) then
   GetDerivedSource=Ui
else
   print*,"Fatal Error : type de fonction indefini"
end if
end function GetDerivedSource

function GeneralFlux(UG,UD,type_function,a)
implicit none 
!Retourne le Flux F_i+1/2 de la formulation des shémas VF pour n'importe quel equation
real*8, intent(in) :: UG,UD
integer, intent(in) :: type_function !=1 pour le transport et =2 pour Burger
real*8, intent(in) :: a
real*8 :: GeneralFlux

!_UG ou _UD pour le point d'évaluation de f 
! (ici pour U_i+1/2 on a  UG=U_i-1 et UD=U_i+1)
!Xd = caractéristiques exprimées grace à Rankine Hugoniot 
!Exemple cours : Xd = 0.5*(U_i+1-U_i) 
!Les indices "d" ici signifient derivee
real*8 :: Xd,fd_UG,fd_UD
!Rankine Hugoniot
!Cette méthode ne marche pas quand UD==UG, ce qui peut etre le cas pour Creneau
!!$  Xd=(GetSource(UD,type_function,a)-GetSource(UG,type_function,a))/(UD-UG)
if (type_function==1) then
   Xd=a
   elseif (type_function==2) then
   Xd=0.5*(UD+UG)
end if

!Calcul de la derivee de f selon le type d'equation
fd_UG=GetDerivedSource(UG,type_function,a)
fd_UD=GetDerivedSource(UD,type_function,a)
!Test pour la construction du flux
!cas 1
if(Xd>0) then
   GeneralFlux=GetSource(UG,type_function,a)
   !cas 2
   elseif(Xd<0.) then
   GeneralFlux=GetSource(UD,type_function,a)
   !cas 3
   elseif(fd_UG>0.) then
   GeneralFlux=GetSource(UG,type_function,a)
   !cas 4
   elseif(fd_UD<0.) then
   GeneralFlux=GetSource(UD,type_function,a)
   !cas 5
   elseif(fd_UG<0. .and. fd_UD>0.) then
   !Flux=f((f')-1(0))
   if (type_function==1) then
      GeneralFlux=a*a 
      elseif (type_function==2) then
      GeneralFlux=0
   end if
end if
end function GeneralFlux

function GetInitialFunction(x0,L,type_init) result (U0)
! Compute the initial condition on a given point 
!   respecting the periodics conditions
implicit none
!Inputs :   + real::L        = size of the interval  
!           + real::x0       = given point
!           + int::type_init = 1 for gaussian, 2 for crenel
real*8,intent(in)::L,x0
integer,intent(in)::type_init

real*8::U0,x
integer::i,nb_intervalle

! On commence par rammener x0 dans l'intervalle -L;L
nb_intervalle=floor(abs(x0)/L)

!Il faut ensuite soustraire à x : 
! L*nb  si nb intervalle pair et L*(nb+1) si intervalle impair
! De plus on utilise signe pour voir si on décalle a droite ou à gauche

x = x0 - sign(nb_intervalle + 1 * mod(nb_intervalle, 2), floor(x0)) * L 
!Sign(a,b) renvoie a affecté du signe de b (il faut b entier car a entier donc floor)

if(type_init == 1) then
   U0 = exp(-(x + 5)**2)
else if(type_init == 2) then
   if (x >- 7.5 .and. x <- 2.5) then
      U0 = 1d0
   else
      U0 = 0d0
   end if
else if (type_init == 3) then
   U0 = sin(4. * 3.14159 * x / L)
end if

end function GetInitialFunction

subroutine periodic(U,n)
implicit none
!Apply peridic conditions : U(0) = U(N) and U(N+1) = U(1)
! for a given vector U of size N+2
real*8,dimension(-1:n+2),intent(inout)::U
integer,intent(in)::n

U(-1) = U(n-1)
U(0) = U(n)
U(n+1) = U(1)
U(n+2) = U(2)
end subroutine

function global_max(Ui_next, U)
   implicit none
   real*8, intent(in) :: Ui_next
   real*8, dimension(:), intent(in) :: U
   real*8 :: Umax, Umin
   logical :: global_max

   Umax = maxval(U)
   Umin = minval(U)

   global_max = (Ui_next >= Umin) .and. (Ui_next <= Umax)
end function

function u2(Ui_next, Ui, Ui_mm, Ui_m, Ui_p, Ui_pp, dx)
   implicit none
   real*8, intent(in) :: Ui_next, Ui, Ui_mm, Ui_m, Ui_p, Ui_pp, dx
   real*8 :: Uxx, Uxx_m, Uxx_p, Uxx_min, Uxx_max, eps
   logical :: DMP, u2

   Uxx =   (Ui_p - 2 * Ui + Ui_m)  / dx**2
   Uxx_m = (Ui - 2 * Ui_m + Ui_mm) / dx**2
   Uxx_p = (Ui_pp - 2 * Ui_p + Ui) / dx**2

   Uxx_min = min(Uxx, Uxx_m, Uxx_p)
   Uxx_max = max(Uxx, Uxx_m, Uxx_p)

   DMP = discret_max(Ui_next, Ui_m, Ui, Ui_p,0.0)
   eps = 1. / 20. ! sqrt(dx)
   u2 = (DMP .eqv. .true.) .or. &
   & ((DMP .eqv. .false.) .and. (Uxx_min * Uxx_max >= 1.e-6) .and. (abs(Uxx_min / Uxx_max) >= 1 - eps))
end function
  
function discret_max(Ui_next,Ui_m,Ui,Ui_p,tol)
    implicit none
    real*8,intent(in)::Ui_next,Ui_m,Ui,Ui_p
    real*8,intent(in)::tol
    logical::discret_max

    real*8::Umax,Umin
  
    Umax = max (Ui_m,Ui,Ui_p)
    Umin = min (Ui_m,Ui,Ui_p)

    discret_max = (Ui_next >= Umin-tol) .and. (Ui_next <= Umax+tol)

  end function

end module Flux
