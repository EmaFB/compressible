#!/bin/bash

function prefix {
if [ $1 -lt 10 ]
then
   s=000;
elif [ $1 -gt 9 ] && [ $1 -lt 100 ]
then
   s=00;
elif [ $1 -gt 99 ] && [ $1 -lt 1000 ]
then
   s=0;
fi
}

function plotU {
   gnuplot << EOF
      set term png
      set grid
      set yrange [-1.1:1.1]

      set output '$1/Mood3_2_1_$2.png'
      plot '$1/Mood3_2_1_$2.dat' w l title 'Iteration $3', 'Initial' w lp title 'Initiale'
EOF
}

function plotOrder {
   gnuplot << EOF
      set term png
      set grid
      set yrange [-0.1:4.1]
      set key bottom right

      set output '$1/Mood3_2_1_$2.png'
      plot '$1/Mood3_2_1_$2.dat' w p title 'Iteration $3'
EOF
}

# Pass the folder where the data is as $1
function animate {
echo $1
list="ls $1/*.dat"
nb_files=0
for l in $list ; do
   let nb_files=nb_files+1
done
let nb_files=nb_files-1
echo "${nb_files} files found"

for ((k = 10 ; k <= $nb_files * 10 ; k+= 10)) ; do
   prefix $k
   i=$s$k
   if [ $k -gt 999 ] && [ $k -lt 10000 ] ; then i=$k ; fi
   `$3 $1 $i $k`
done
convert $1/*[0-9].png $2
}

animate Animation U.gif 'plotU'
animate Animation_order U_order.gif 'plotOrder'
