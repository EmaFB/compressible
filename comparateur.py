# -*- coding: utf-8 -*-

# Utilitaire de comparaison de deux résultats
import numpy as np
import argparse
def norme2(U):
  return np.sqrt( sum( [x*x for x in U] ) )
def normeinf(U):
  return max( [abs(x) for x in U] )

parser = argparse.ArgumentParser()
parser.add_argument('-f',nargs=2)
parser.add_argument('-n',nargs=1,type=int)
args = parser.parse_args()

file1 = args.f[0]
file2 = args.f[1]
num_col = args.n[0]

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)
V1 = data1[:,num_col]
V2 = data2[:,num_col]
print "Norme 2 de la difference = ", norme2(V1-V2)
print "Norme infinie de la difference = ", normeinf(V1-V2), "atteint à la ligne", np.argmax(abs(V1-V2))


